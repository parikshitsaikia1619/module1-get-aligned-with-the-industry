# DNN Summary

### What is a neural network?

A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes.

![pic](https://databricks.com/wp-content/uploads/2019/02/neural1.jpg)

A basic neural network has 3 layers:

* **input layer**:  This is the first layer in the neural network. It takes input signals(values) and passes them on to the next layer. It doesn’t apply any operations on the input signals(values) & has no weights and biases values associated
* **hidden layer**: Hidden layers have neurons(nodes) which apply different transformations to the input data. One hidden layer is a collection of neurons stacked vertically(Representation).  Last hidden layer passes on values to the output layer. All the neurons in a hidden layer are connected to each and every neuron in the next layer, hence we have a fully connected hidden layers.
* **output layer**: This layer is the last layer in the network & receives input from the last hidden layer. With this layer we can get desired number of values and in a desired range.


**Perceptron**: It is a simpler version of neural network, Its working principle is same as neural network.

![percep](https://images.deepai.org/glossary-terms/perceptron-6168423.jpg)

### what is DNN?

A deep neural network (DNN) is an artificial neural network (ANN) with multiple layers between the input and output layers. The DNN finds the correct mathematical manipulation to turn the input into the output, whether it be a linear relationship or a non-linear relationship.

![DNN](https://www.oreilly.com/library/view/python-natural-language/9781787121423/assets/70fd1f89-7609-4bdc-86e2-a8786869b8be.png)

### How Neural network learns?

* **Forward Propagation** — Forward propagation is a process of feeding input values to the neural network and getting an output which we call predicted value. When we feed the input values to the neural network’s first layer, it goes without any operations. Second layer takes values from first layer and applies multiplication, addition and activation operations and passes this value to the next layer. Same process repeats for subsequent layers and finally we get an output value from the last layer.

* **Back-Propagation** — After forward propagation we get an output value which is the predicted value. To calculate error we compare the predicted value with the actual output value. We use a **loss function** to calculate the error value. Then we calculate the derivative of the error value with respect to each and every weight in the neural network. Back-Propagation uses chain rule of Differential Calculus. We repeat this process until we get gradients for each and every weight in our neural network.


* **Model Optimizers** — The optimizer is a search technique, which is used to update weights in the model.
    - **SGD**: Stochastic Gradient Descent, with support for momentum.
    - **RMSprop**: Adaptive learning rate optimization method proposed by Geoff Hinton.
    - **Adam**: Adaptive Moment Estimation (Adam) that also uses adaptive learning rates.