# CNN Summary

### What is CNN?

* A convolutional neural network (CNN) is a specific type of artificial neural network that uses perceptrons, a machine learning unit algorithm, for supervised learning, to analyze data. CNNs apply to image processing, natural language processing and other kinds of cognitive tasks.
* A convolutional neural network is also known as a ConvNet.

![CNn](https://miro.medium.com/max/3744/1*SGPGG7oeSvVlV5sOSQ2iZw.png)


### Terminologies of CNN

* **convolution Layer**: Convolutional Layers have a moving filter also called as the weight matrix. The filter slides over the input image (convolution operation) to produce a feature map.
Weight matrix and Input Image multiply (dot product) and summed to produce the feature map.

![feature](https://miro.medium.com/max/2292/1*u2el-HrqRPVk7x0xlvs_CA.png)

* **Max Pooling Layer** : We move a window across a 2D input space, where the maximum value within that window is the output.It is also called as downsampling layer because it reduces the number of parameters within the model.

![max pool](https://computersciencewiki.org/images/8/8a/MaxpoolSample2.png)

* **Droput Layer**: Dropout refers to dropping(not considering in both forward and backward pass) some neurons during the training phase. The neurons which are chosen at random. 
![drop](https://miro.medium.com/fit/c/1638/491/1*B7nBQ1DoodxrNUgTVxNesQ.png)

* **Flatten Layer**: In between the convolutional layer and the fully connected layer, there is a ‘Flatten’ layer. Flattening transforms a two-dimensional matrix of features into a vector that can be fed into a fully connected neural network classifier.
![flatten](https://missinglink.ai/wp-content/uploads/2019/04/Group-5.png)

* **Fully Connected Layer** : also known as the dense layer, in which the results of the convolutional layers are fed through one or more neural layers to generate a prediction.
![ala](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/74_blog_image_2.png)

